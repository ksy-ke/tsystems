package com.ksy.tsystems.api;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.ksy.tsystems.api.TestUtils.getResourceContent;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class SpellNumberTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void validSpelling() throws Exception {
        mvc.perform(get("/api/v1/spell-number")
                .content(getResourceContent("/spell_number/valid_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(getResourceContent("/spell_number/valid_response.json")));
    }

    @Test
    public void unknownField() throws Exception {
        mvc.perform(get("/api/v1/spell-number")
                .content(getResourceContent("/spell_number/unknown_field_bad_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void alphabeticSymbols() throws Exception {
        mvc.perform(get("/api/v1/spell-number")
                .content(getResourceContent("/spell_number/alphabetic_symbols_bad_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void specialSymbols() throws Exception {
        mvc.perform(get("/api/v1/spell-number")
                .content(getResourceContent("/spell_number/special_symbols_bad_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void noData() throws Exception {
        mvc.perform(get("/api/v1/spell-number")
                .content(getResourceContent("/spell_number/no_data_bad_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}