package com.ksy.tsystems.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.ksy.tsystems.api.TestUtils.getResourceContent;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class MatchStringTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void validStringAndPattern() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/valid_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(getResourceContent("/match_string/valid_response.json")));
    }

    @Test
    public void emptyString() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/empty_string_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void stringWithNonEnglishLetters() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/string_with_non_English_letters_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void stringWitWhitespace() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/string_with_whitespace_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void emptyPatternsList() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/empty_patterns_list_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void invalidPatternsList() throws Exception {
        mvc.perform(get("/api/v1/match-string")
                .content(getResourceContent("/match_string/invalid_patterns_list_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}