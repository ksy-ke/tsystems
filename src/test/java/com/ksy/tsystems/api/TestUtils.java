package com.ksy.tsystems.api;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static java.nio.file.Files.readString;

public final class TestUtils {
    private TestUtils() { }

    public static String getResourceContent(String name) throws URISyntaxException, IOException {
        return readString(Paths.get(TestUtils.class.getResource(name).toURI()));
    }
}
