package com.ksy.tsystems.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.ksy.tsystems.api.TestUtils.getResourceContent;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class MultiplesSumTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void validInput() throws Exception {
        mvc.perform(get("/api/v1/multiples-sum")
                .content(getResourceContent("/multiples_sum/valid_numbers_request.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(getResourceContent("/multiples_sum/valid_numbers_response.json")));
    }

    @Test
    public void invalidNegativeNumbers() throws Exception {
        mvc.perform(get("/api/v1/multiples-sum")
                .content(getResourceContent("/multiples_sum/invalid_negative_numbers.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void invalidZero() throws Exception {
        mvc.perform(get("/api/v1/multiples-sum")
                .content(getResourceContent("/multiples_sum/invalid_zero.json"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}