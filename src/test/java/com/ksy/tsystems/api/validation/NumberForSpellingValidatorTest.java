package com.ksy.tsystems.api.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class NumberForSpellingValidatorTest {
    private final NumberForSpellingValidator validator = new NumberForSpellingValidator();

    @MethodSource("values")
    @ParameterizedTest
    void validationNumber(String value, boolean expected) {
        // when
        boolean actual = validator.isValid(value, null);

        // then
        assertEquals(expected, actual);
    }

    private static List<Arguments> values() {
        return List.of(
                arguments("23", true),
                arguments("23,000", true),
                arguments("23.20", false),
                arguments("un", false),
                arguments("", false),
                arguments(null, false)
        );
    }
}