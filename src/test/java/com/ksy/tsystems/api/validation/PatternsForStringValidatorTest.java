package com.ksy.tsystems.api.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class PatternsForStringValidatorTest {
    private final PatternsForStringValidator validator = new PatternsForStringValidator();

    @ParameterizedTest
    @MethodSource("patterns")
    void validationPattern(List<String> patterns, boolean expected) {
        // when
        boolean actual = validator.isValid(patterns, null);

        // then
        assertEquals(expected, actual);
    }

    private static List<Arguments> patterns() {
        return List.of(
                arguments(List.of("\\d"), true),
                arguments(List.of("\\a", "\\s"), true),
                arguments(List.of("\\t"), false),
                arguments(List.of("\\a", "\\r"), false)
        );
    }
}