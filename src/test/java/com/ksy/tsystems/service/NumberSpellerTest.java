package com.ksy.tsystems.service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class NumberSpellerTest {
    private final NumberSpeller numberSpeller = new NumberSpeller();

    @MethodSource("number_to_literals")
    @ParameterizedTest
    public void positiveTest(int number, String expected) {
        // when
        String actual = numberSpeller.convertNumber(number);
        // then
        assertEquals(expected, actual);
    }

    private static List<Arguments> number_to_literals() {
        return List.of(
                arguments(0, "zero"),
                arguments(5, "five"),
                arguments(12, "twelve"),
                arguments(14, "fourteen"),
                arguments(152, "one hundred and fifty-two"),
                arguments(200, "two hundred"),
                arguments(3_043, "three thousand, forty-three"),
                arguments(85_000, "eighty-five thousand"),
                arguments(45_342, "forty-five thousand, three hundred and forty-two"),
                arguments(900_562, "nine hundred thousand, five hundred and sixty-two"),
                arguments(999_000, "nine hundred and ninety-nine thousand"),
                arguments(47_483_647, "forty-seven million, four hundred and eighty-three thousand, six hundred and forty-seven"),
                arguments(2_147_483_647, "two billion, one hundred and forty-seven million, four hundred and eighty-three thousand, six hundred and forty-seven"),
                arguments(-47_483_647, "minus forty-seven million, four hundred and eighty-three thousand, six hundred and forty-seven")
        );
    }
}