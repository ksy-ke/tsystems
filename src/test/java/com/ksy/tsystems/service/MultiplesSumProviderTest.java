package com.ksy.tsystems.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ValidationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class MultiplesSumProviderTest {
    private final MultiplesSumProvider multiplesSumProvider = new MultiplesSumProvider();

    @MethodSource("multiples_limit_and_numbers")
    @ParameterizedTest
    public void positiveTest(int limit, int firstNumber, int secondNumber, int expectedFirstSum, int expectedSecondSum, int expectedSum) {
        // when
        var actual = multiplesSumProvider.count(limit, firstNumber, secondNumber);

        // then
        assertEquals(expectedFirstSum, actual.getSumFirstNumber());
        assertEquals(expectedSecondSum, actual.getSumSecondNumber());
        assertEquals(expectedSum, actual.getSum());
    }

    @Test
    public void limitBelowNumbers() {
        // given
        int limit = 5;
        int firstNumber = 10;
        int secondNumber = 14;

        // when
        Executable invocation = () -> multiplesSumProvider.count(limit, firstNumber, secondNumber);

        // then
        assertThrows(ValidationException.class, invocation);
    }

    private static List<Arguments> multiples_limit_and_numbers() {
        return List.of(
                arguments(5, 2, 3, 6, 3, 9),
                arguments(6, 5, 5, 5, 5, 5),
                arguments(10, 2, 3, 20, 18, 32),
                arguments(125, 7, 60, 1071, 180, 1251),
                arguments(200, 5, 7, 3900, 2842, 6217)
        );
    }
}