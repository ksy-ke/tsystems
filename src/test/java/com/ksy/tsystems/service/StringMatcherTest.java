package com.ksy.tsystems.service;

import com.ksy.tsystems.api.model.response.MatchStringResponse;
import com.ksy.tsystems.api.model.response.MatchStringResponse.LengthForPattern;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class StringMatcherTest {
    private final StringMatcher stringMatcher = new StringMatcher();

    @MethodSource("string_for_matching")
    @ParameterizedTest
    public void positiveTest(String str, List<String> pattern, MatchStringResponse expectedResponse) {
        // when
        MatchStringResponse actual = stringMatcher.findSequencesAndCheckPattern(str, pattern);
        // then
        assertEquals(expectedResponse.getInput(), actual.getInput());
        assertEquals(expectedResponse.getSequences(), actual.getSequences());
        assertEquals(expectedResponse.getLengthForPatterns(), actual.getLengthForPatterns());
    }

    private static List<Arguments> string_for_matching() {
        return of(
                arguments("qr12qw", of("\\a"), new MatchStringResponse("qr12qw", of(), of(new LengthForPattern("\\a", 2)))),
                arguments("abca123", of("\\d", "\\s"), new MatchStringResponse("abca123", of("abca", "123"), of(new LengthForPattern("\\d", 3), new LengthForPattern("\\s", 0)))),
                arguments("abca*%&#(%123", of("\\a", "\\s"), new MatchStringResponse("abca*%&#(%123", of("abca", "123"), of(new LengthForPattern("\\a", 4), new LengthForPattern("\\s", 6)))),
                arguments("12qr#4321", of("\\a", "\\d", "\\s"), new MatchStringResponse("12qr#4321", of("4321"),
                        of(new LengthForPattern("\\a", 2), new LengthForPattern("\\d", 4), new LengthForPattern("\\s", 1))))
        );
    }
}