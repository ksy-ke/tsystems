package com.ksy.tsystems.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.ResponseEntity.badRequest;

/** Transforms exception to responses. */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        var errors = concat(
                ex.getBindingResult().getFieldErrors().stream()
                        .map(error -> error.getField() + ": " + error.getDefaultMessage()),
                ex.getBindingResult().getGlobalErrors().stream()
                        .map(error -> error.getObjectName() + ": " + error.getDefaultMessage())
        ).collect(toList());

        return handleExceptionInternal(ex, new ErrorBody(errors), headers, BAD_REQUEST, request);
    }

    /**
     * Transforms {@link ValidationException} to response with exception message.
     *
     * @param ex exception to transform
     * @return response with exception message
     */
    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ValidationException ex) {
        return badRequest().body(new ErrorBody(List.of(ex.getLocalizedMessage())));
    }

    /** Response body with reasoning. */
    private static class ErrorBody {
        /** Reasons of exceptions. */
        private final List<String> reasons;

        /**
         * Class constructor specifying:
         *
         * @param reasons {@link #reasons}
         */
        private ErrorBody(List<String> reasons) { this.reasons = reasons; }

        /** @see #reasons */
        public List<String> getReasons() { return reasons; }
    }
}
