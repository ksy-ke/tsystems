package com.ksy.tsystems.api;

import com.ksy.tsystems.api.model.request.MatchStringRequest;
import com.ksy.tsystems.api.model.request.SpellNumberRequest;
import com.ksy.tsystems.api.model.request.MultiplesSumRequest;
import com.ksy.tsystems.api.model.response.MatchStringResponse;
import com.ksy.tsystems.api.model.response.SpellNumberResponse;
import com.ksy.tsystems.api.model.response.MultiplesSumResponse;
import com.ksy.tsystems.service.NumberSpeller;
import com.ksy.tsystems.service.StringMatcher;
import com.ksy.tsystems.service.MultiplesSumProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static java.lang.Integer.parseInt;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/** Provides API for calculation and matching. */
@RestController
@RequestMapping("/api/v1")
public class TaskController {
    /** Allows to convert number to text. */
    private final NumberSpeller numberSpeller;
    /** Allows to count sum of multiples of numbers. */
    private final MultiplesSumProvider multiplesSumProvider;
    /** Allows to match strings by patterns. */
    private final StringMatcher stringMatcher;

    /**
     * Class constructor specifying:
     *
     * @param numberSpeller        {@link #numberSpeller}
     * @param multiplesSumProvider {@link #multiplesSumProvider}
     * @param stringMatcher{@link  #stringMatcher}
     */
    @Autowired
    public TaskController(NumberSpeller numberSpeller, MultiplesSumProvider multiplesSumProvider, StringMatcher stringMatcher) {
        this.numberSpeller = numberSpeller;
        this.multiplesSumProvider = multiplesSumProvider;
        this.stringMatcher = stringMatcher;
    }

    /**
     * Spells specified number.
     *
     * @param request contains string to spell
     * @return spelled string
     */
    @RequestMapping(value = "/spell-number", method = GET, consumes = {APPLICATION_JSON_VALUE})
    public SpellNumberResponse getSpellNumber(@Valid @RequestBody SpellNumberRequest request) {
        int number = parseInt(request.getNumber().replace(",", ""));
        return new SpellNumberResponse(numberSpeller.convertNumber(number));
    }

    /**
     * Returns sum of multiples for numbers.
     *
     * @param request contains numbers and limit
     * @return sums
     */
    @RequestMapping(value = "/multiples-sum", method = GET, consumes = {APPLICATION_JSON_VALUE})
    public MultiplesSumResponse getMultiplesSum(@Valid @RequestBody MultiplesSumRequest request) {
        return multiplesSumProvider.count(request.getLimit(), request.getFirstNumber(), request.getSecondNumber());
    }

    /**
     * Returns strings matches.
     *
     * @param request string and patterns
     * @return matching result
     */
    @RequestMapping(value = "/match-string", method = GET, consumes = {APPLICATION_JSON_VALUE})
    public MatchStringResponse matchString(@Valid @RequestBody MatchStringRequest request) {
        return stringMatcher.findSequencesAndCheckPattern(request.getInput(), request.getPatterns());
    }
}