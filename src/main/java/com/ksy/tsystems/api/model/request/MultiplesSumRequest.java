package com.ksy.tsystems.api.model.request;

import javax.validation.constraints.Positive;

/** Input two numbers to search for sum of multiples of it and limit for finding multiples of. */
public class MultiplesSumRequest {
    /** Message for exception, when found invalid data. */
    public static final String INVALID_PARAMETER_MESSAGE = "Should be positive, not nil numbers";
    /** Limit for finding multiples of two numbers: {@link #firstNumber} and {@link #secondNumber}. */
    @Positive(message = INVALID_PARAMETER_MESSAGE)
    private int limit;
    /** Number for search all multiples below given {@link #limit} and sum them. */
    @Positive(message = INVALID_PARAMETER_MESSAGE)
    private int firstNumber;
    /** Number for search all multiples below given {@link #limit} and sum them. */
    @Positive(message = INVALID_PARAMETER_MESSAGE)
    private int secondNumber;

    /** @see #limit */
    public int getLimit() { return limit; }

    /** @see #firstNumber */
    public int getFirstNumber() { return firstNumber; }

    /** @see #secondNumber */
    public int getSecondNumber() { return secondNumber; }
}