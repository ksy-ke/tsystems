package com.ksy.tsystems.api.model.request;

import com.ksy.tsystems.api.validation.NumberForSpelling;

/** Input number string to transform into words. */
public class SpellNumberRequest {
    /** Input string with number. Should be contains int. Can't contains letters, whitespaces or specific symbols (excluding ","). */
    @NumberForSpelling
    private String number;

    /** @see #number */
    public String getNumber() { return number; }
}