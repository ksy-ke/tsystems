package com.ksy.tsystems.api.model.request;

import com.ksy.tsystems.api.validation.PatternsForString;
import com.ksy.tsystems.service.StringMatcher;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

/** Input string and patterns for matching. */
public class MatchStringRequest {
    /**
     * Input string for matching.
     * Can contains English alphabet letters, special symbols and digits.
     * Can't contains other alphabets letters or whitespaces.
     */
    @NotBlank(message = "Input string is mandatory")
    @Pattern(regexp = "\\w+", message = "Input string contains non English alphabet letters or whitespace")
    private String input;

    /** Input list of patterns for matching string. Patterns should be contained in {@link StringMatcher#PATTERNS}. */
    @NotEmpty(message = "Send at least one pattern")
    @PatternsForString
    private List<String> patterns;

    /** @see #input */
    public String getInput() { return input; }

    /** @see #patterns */
    public List<String> getPatterns() { return patterns; }
}