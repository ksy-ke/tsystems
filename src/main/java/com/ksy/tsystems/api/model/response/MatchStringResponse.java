package com.ksy.tsystems.api.model.response;

import java.util.List;
import java.util.Objects;

/** String matching result. */
public class MatchStringResponse {
    /** Input string from request. */
    private final String input;
    /** List of all found continuous sequences of literals and numbers whose length is greater than or equal to 3. */
    private final List<String> sequences;
    /** Results list whose contains length for each input pattern. */
    private final List<LengthForPattern> lengthForPatterns;

    /**
     * Class constructor specifying:
     *
     * @param input             {@link #input}
     * @param sequences         {@link #sequences}
     * @param lengthForPatterns {@link #lengthForPatterns}
     */
    public MatchStringResponse(String input, List<String> sequences, List<LengthForPattern> lengthForPatterns) {
        this.input = input;
        this.sequences = sequences;
        this.lengthForPatterns = lengthForPatterns;
    }

    /** @see #input */
    public String getInput() { return input; }

    /** @see #sequences */
    public List<String> getSequences() { return sequences; }

    /** @see #lengthForPatterns */
    public List<LengthForPattern> getLengthForPatterns() { return lengthForPatterns; }

    public static class LengthForPattern {
        /** Pattern whose used for matching. */
        private final String pattern;
        /** Max matches length for {@link #pattern} */
        private final int max;

        /**
         * Class constructor specifying:
         *
         * @param max     {@link #max}
         * @param pattern {@link #pattern}
         */
        public LengthForPattern(String pattern, int max) {
            this.pattern = pattern;
            this.max = max;
        }

        /** @see #pattern */
        public String getPattern() { return pattern; }

        /** @see #max */
        public int getMax() { return max; }

        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LengthForPattern that = (LengthForPattern) o;
            return max == that.max && Objects.equals(pattern, that.pattern);
        }

        @Override public int hashCode() { return Objects.hash(pattern, max); }
    }
}
