package com.ksy.tsystems.api.model.response;

/** Result convert number to word. */
public class SpellNumberResponse {
    /** Result convert number to word. */
    private final String result;

    /**
     * Class constructor specifying:
     *
     * @param result {@link #result}
     */
    public SpellNumberResponse(String result) { this.result = result; }

    /** @see #result */
    public String getResult() { return result; }
}
