package com.ksy.tsystems.api.model.response;

/** The found sum of all multiples of two numbers below a given limit and sum all of them */
public class MultiplesSumResponse {
    /** Sum of all the multiples of the first number below given limit. */
    private final int sumFirstNumber;
    /** Sum of all the multiples of the second number below given limit. */
    private final int sumSecondNumber;
    /** Sum of all the multiples of the first or the second number below given limit. */
    private final int sum;

    /**
     * Class constructor specifying:
     *
     * @param sumFirstNumber  {@link #sumFirstNumber}
     * @param sumSecondNumber {@link #sumSecondNumber}
     * @param sum             {@link #sum}
     */
    public MultiplesSumResponse(int sumFirstNumber, int sumSecondNumber, int sum) {
        this.sumFirstNumber = sumFirstNumber;
        this.sumSecondNumber = sumSecondNumber;
        this.sum = sum;
    }

    /** @see #sumFirstNumber */
    public int getSumFirstNumber() { return sumFirstNumber; }

    /** @see #sumSecondNumber */
    public int getSumSecondNumber() { return sumSecondNumber; }

    /** @see #sum */
    public int getSum() { return sum; }
}