package com.ksy.tsystems.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.lang.Integer.parseInt;

/** Validator for input string. */
public class NumberForSpellingValidator implements ConstraintValidator<NumberForSpelling, String> {
    /**
     * Validation input string, shouldn't be contains letters, whitespaces or specific symbols (excluding ",").
     * Can't be null.
     * Can be parse to int.
     *
     * @param value   string for validate
     * @param context validation context
     * @return string valid or not
     */
    @Override public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return false;
        value = value.replace(",", "");
        try {
            parseInt(value);
            return true;
        } catch (NumberFormatException e) { return false; }
    }
}
