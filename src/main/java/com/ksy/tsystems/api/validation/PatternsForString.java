package com.ksy.tsystems.api.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/** Validates that string contains only known patterns. */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = PatternsForStringValidator.class)
public @interface PatternsForString {
    String message() default "Unknown pattern. Should be \\d, \\a or \\s";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}