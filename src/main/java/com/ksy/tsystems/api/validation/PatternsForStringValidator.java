package com.ksy.tsystems.api.validation;

import com.ksy.tsystems.service.StringMatcher;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

import static com.ksy.tsystems.service.StringMatcher.PATTERNS;

/** Validator for input list of patterns. */
public class PatternsForStringValidator implements ConstraintValidator<PatternsForString, List<String>> {

    /**
     * Validation input list of patterns, each pattern should be contained in {@link StringMatcher#PATTERNS}
     *
     * @param values  list of patterns for validate
     * @param context validation context
     * @return all patterns valid or not.
     */
    @Override public boolean isValid(List<String> values, ConstraintValidatorContext context) {
        return PATTERNS.keySet().containsAll(values);
    }
}
