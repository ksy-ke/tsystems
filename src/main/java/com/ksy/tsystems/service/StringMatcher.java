package com.ksy.tsystems.service;

import com.ksy.tsystems.api.model.response.MatchStringResponse;
import com.ksy.tsystems.api.model.response.MatchStringResponse.LengthForPattern;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;

import static java.util.Collections.unmodifiableMap;
import static java.util.regex.Pattern.compile;
import static java.util.stream.Collectors.toList;

/** Provides matching for passed string and patterns. */
@Service
public class StringMatcher {
    /** Allowed patterns for matching. */
    public final static Map<String, String> PATTERNS = unmodifiableMap(Map.of(
            "\\d", "\\p{Digit}",
            "\\a", "\\p{Alpha}",
            "\\s", "\\p{Punct}"
    ));

    /**
     * Provides matching for specified input based on list of patters.
     *
     * @param input    value to run matching against
     * @param patterns patters to use for matching based on {@link #PATTERNS}
     * @return matched data
     */
    public MatchStringResponse findSequencesAndCheckPattern(String input, List<String> patterns) {
        List<LengthForPattern> patternToMax = patterns.stream()
                .map((String pattern) -> findMaxForPattern(input, pattern))
                .collect(toList());
        return new MatchStringResponse(input, findLiteralAndNumbersSequences(input), patternToMax);
    }

    /**
     * Provides all found continuous sequences of literals and numbers.
     *
     * @param input string to analise
     * @return found literals and digits that has length greater or equal to 3
     */
    private List<String> findLiteralAndNumbersSequences(String input) {
        String patternForNumbers = "[" + PATTERNS.get("\\d") + "]{3,}";
        String patternForLetter = "[" + PATTERNS.get("\\a") + "]{3,}";
        return compile(patternForNumbers + "|" + patternForLetter)
                .matcher(input)
                .results()
                .map(MatchResult::group)
                .collect(toList());
    }

    /**
     * Provides max length of matched character sequence.
     *
     * @param input   string to analise
     * @param pattern pattern to find length for
     * @return pattern and max matched length
     */
    private LengthForPattern findMaxForPattern(String input, String pattern) {
        int result = compile("[" + PATTERNS.get(pattern) + "]+")
                .matcher(input)
                .results()
                .map(MatchResult::group)
                .mapToInt(String::length)
                .max()
                .orElse(0);
        return new LengthForPattern(pattern, result);
    }
}