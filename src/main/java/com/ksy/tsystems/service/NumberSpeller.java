package com.ksy.tsystems.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.util.Map.of;

/** Provides conversion of number into words. */
@Service
public class NumberSpeller {
    /** Name of degrees in int range. */
    private final static Map<Integer, String> DEGREES = of(
            1, " thousand",
            2, " million",
            3, " billion"
    );

    /** Numbers (range 1-20 and every ten to one hundred) to words representation. */
    private final static Map<Integer, String> NUMBERS_TO_WORDS_VIEW = new HashMap<>();

    static {
        NUMBERS_TO_WORDS_VIEW.put(1, "one");
        NUMBERS_TO_WORDS_VIEW.put(2, "two");
        NUMBERS_TO_WORDS_VIEW.put(3, "three");
        NUMBERS_TO_WORDS_VIEW.put(4, "four");
        NUMBERS_TO_WORDS_VIEW.put(5, "five");
        NUMBERS_TO_WORDS_VIEW.put(6, "six");
        NUMBERS_TO_WORDS_VIEW.put(7, "seven");
        NUMBERS_TO_WORDS_VIEW.put(8, "eight");
        NUMBERS_TO_WORDS_VIEW.put(9, "nine");
        NUMBERS_TO_WORDS_VIEW.put(10, "ten");
        NUMBERS_TO_WORDS_VIEW.put(11, "eleven");
        NUMBERS_TO_WORDS_VIEW.put(12, "twelve");
        NUMBERS_TO_WORDS_VIEW.put(13, "thirteen");
        NUMBERS_TO_WORDS_VIEW.put(14, "fourteen");
        NUMBERS_TO_WORDS_VIEW.put(15, "fifteen");
        NUMBERS_TO_WORDS_VIEW.put(16, "sixteen");
        NUMBERS_TO_WORDS_VIEW.put(17, "seventeen");
        NUMBERS_TO_WORDS_VIEW.put(18, "eighteen");
        NUMBERS_TO_WORDS_VIEW.put(19, "nineteen");
        NUMBERS_TO_WORDS_VIEW.put(20, "twenty");
        NUMBERS_TO_WORDS_VIEW.put(30, "thirty");
        NUMBERS_TO_WORDS_VIEW.put(40, "forty");
        NUMBERS_TO_WORDS_VIEW.put(50, "fifty");
        NUMBERS_TO_WORDS_VIEW.put(60, "sixty");
        NUMBERS_TO_WORDS_VIEW.put(70, "seventy");
        NUMBERS_TO_WORDS_VIEW.put(80, "eighty");
        NUMBERS_TO_WORDS_VIEW.put(90, "ninety");
    }

    /**
     * Provides conversion of number into words.
     *
     * @param number number to convert
     * @return result of conversion of number into word
     */
    public String convertNumber(int number) {
        var result = new StringBuilder();
        int numberOfDivisions = 0;

        if (number == 0) return "zero";

        int count = abs(number);
        while (count > 0) {
            String str = matchThousandPart(count % 1000);
            count /= 1000;
            numberOfDivisions++;
            if (str.equals("")) continue;
            if (result.toString().equals(""))
                result.insert(0, str + DEGREES.getOrDefault(numberOfDivisions - 1, ""));
            else
                result.insert(0, str + DEGREES.getOrDefault(numberOfDivisions - 1, "") + ", ");
        }

        return number < 0 ? "minus " + result.toString() : result.toString();
    }

    /**
     * Provides string representation for number < 1000.
     *
     * @param number number < 1000 to get string representation for
     * @return string representation
     */
    private String matchThousandPart(int number) {
        String dozen = matchHundredPart(number % 100);
        int hundred = number / 100;
        String result = "";
        if (hundred != 0) result = NUMBERS_TO_WORDS_VIEW.get(hundred) + " hundred";
        if (dozen.equals("")) return result;
        return result.equals("") ? dozen : result + " and " + dozen;
    }

    /**
     * Provides string representation for number < 100.
     *
     * @param number number < 100 to get string representation for
     * @return string representation
     */
    private String matchHundredPart(int number) {
        if (number == 0) return "";
        if (number < 20) return NUMBERS_TO_WORDS_VIEW.get(number);
        int units = number % 10;
        int tenner = number - units;

        return units == 0 ? NUMBERS_TO_WORDS_VIEW.get(tenner) : NUMBERS_TO_WORDS_VIEW.get(tenner) + "-" + NUMBERS_TO_WORDS_VIEW.get(units);
    }
}