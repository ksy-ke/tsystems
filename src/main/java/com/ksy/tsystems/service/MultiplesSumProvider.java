package com.ksy.tsystems.service;

import com.ksy.tsystems.api.model.response.MultiplesSumResponse;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.HashSet;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.iterate;

/** Provides multiples sum counting below given limit of two numbers */
@Service
public class MultiplesSumProvider {
    /**
     * Find multiples of numbers and counts sum of them.
     *
     * @param limit  limit for finding multiples of two numbers
     * @param first  number to find all multiples below given limit and sum them
     * @param second number to find all multiples below given limit and sum them
     * @return response with sum of all multiples
     */
    public MultiplesSumResponse count(int limit, int first, int second) {
        if (limit < first || limit < second) throw new ValidationException("Limit is below one of numbers");
        var multiplesOfFirstNumber = findMultiplesWithLimit(limit, first);
        var multiplesOfSecondNumber = findMultiplesWithLimit(limit, second);

        return new MultiplesSumResponse(calculateAmount(multiplesOfFirstNumber),
                calculateAmount(multiplesOfSecondNumber),
                calculateAmount(multiplesOfFirstNumber, multiplesOfSecondNumber));
    }

    /**
     * Find multiples of number.
     *
     * @param limit  limit for finding multiples of number
     * @param number number to find all multiples below given limit
     * @return result list with all multiples of number below limit
     */
    private List<Integer> findMultiplesWithLimit(int limit, int number) {
        return iterate(number, i -> i < limit, i -> i + number)
                .collect(toList());
    }

    /**
     * Counts the sum of all multiples of number.
     *
     * @param multiples list with all multiples of number below limit
     * @return sum of all multiples of number
     */
    private int calculateAmount(List<Integer> multiples) {
        return multiples.stream().mapToInt(Integer::intValue).sum();
    }

    /**
     * Counts the sum of unique multiples of two numbers.
     *
     * @param multiplesOfFirstNumber  result list with all multiples of first number below limit
     * @param multiplesOfSecondNumber result list with all multiples of second number below limit
     * @return sum of unique multiples of two numbers
     */
    private int calculateAmount(List<Integer> multiplesOfFirstNumber, List<Integer> multiplesOfSecondNumber) {
        var multiples = new HashSet<Integer>();
        multiples.addAll(multiplesOfFirstNumber);
        multiples.addAll(multiplesOfSecondNumber);
        return multiples.stream().mapToInt(Integer::intValue).sum();
    }
}
