# README #

* Author name : Kseniya Shatokhina/Ксения Шатохина
* Task 1 : `/api/v1/spell-number`   `{"number": "152"}`
* Task 2 : `/api/v1/multiples-sum`  `{"limit": "5", "firstNumber": "2", "secondNumber": "3"}`  // Only positive numbers are allowed.
* Task 3 : `/api/v1/match-string`   `{"input": "abca123", "patterns": ["\\d","\\s"]}`

I was confused by examples of third task, hope I got it right.

Probably it's an issue in example:
`"qwer12qw -> empty array as second parameter"`.<br>
But description tells: `all found letter or number sequences ... minimum 3 characters`.<br>
And `qwer` has four letters length, so my program returns it. 

More requests for tests in /src/test/resources